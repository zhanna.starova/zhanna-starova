package hw13;// Java program to check if given mobile number
// is valid.

import java.util.regex.*;
import java.util.Scanner;

class NumberValidator {

    public static boolean isValid(String ssn) {
        //Initialize regex for SSN.
        String expression = "^(80|380|\\+380|0|00380)(50|63|66|67|68|73|91|92|93|94|95|96|97|98|99)([0-9]){7}$";
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(ssn);

        return matcher.matches();
    }

    public static void amain(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input phone number: ");
        String s = in.nextLine();

        while (!isValid(s)) {
            System.out.println("Invalid Number");
            in = new Scanner(System.in);
            System.out.print("Input phone number: ");
            s = in.nextLine();
        }
        System.out.println("Valid Number");
    }
}
