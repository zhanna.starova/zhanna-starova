package hw14;

public class Triangle implements Shape {
	private double a;
	private double b;
	private double c;

	public Triangle(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}


	public double calculateArea() {
		double p = (a + b + c)/2;
		return Math.sqrt(p * (p - a) * (p - b) * (p - c));
	}


	public double calculatePerimeter() {
		return a + b + c;

	}
}
