package hw14;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		double a;
		double b;
		double c;

		Scanner in = new Scanner(System.in);
		System.out.println("Enter shape");
		String shape1 = in.nextLine().toUpperCase();

		ShapeType shapeType = ShapeType.valueOf(shape1);

		Shape shape = null;


		switch (shapeType){
			case CIRCLE:
				System.out.println("Enter radius: ");
				double radius = in.nextDouble();
				shape = new Circle(radius);
				break;
			case SQUARE:
				System.out.println("Input a: ");
				a = in.nextDouble();
				shape = new Square(a);
				break;
			case TRIANGLE:
				System.out.println("Input a: ");
				a = in.nextDouble();
				System.out.println("Input b: ");
				b = in.nextDouble();
				System.out.println("Input c: ");
				c = in.nextDouble();
				shape = new Triangle(a, b, c);
				break;
			case RECTANGLE:
				System.out.println("Input a: ");
				a = in.nextDouble();
				System.out.println("Input b: ");
				b = in.nextDouble();
				shape = new Rectangle(a, b);
				break;
		}

		System.out.println(shape.calculateArea());
		System.out.println(shape.calculatePerimeter());
	}
}
