package hw14;


public class Square implements Shape {
	private double a;

	public Square(double a) {
		this.a = a;
	}


	public double calculateArea() {
		return a * a;

	}

	public double calculatePerimeter() {
		return 4 * a;
	}
}
