package hw14;

public interface Shape {
    double calculateArea();
    double calculatePerimeter();
}
